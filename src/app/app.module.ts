import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { InicioComponent } from './components/inicio/inicio.component';


//Rutas
import { APP_ROUTING } from './app.routes';
import { RouterModule } from '@angular/router';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { ProfesionalesComponent } from './components/profesionales/profesionales.component';
import { ServiciosComponent } from './components/servicios/servicios.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';



@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    NavBarComponent,
    InicioComponent,
    NosotrosComponent,
    ProfesionalesComponent,
    ServiciosComponent,
    ContactoComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }